# Prolific PL2303HXD USB-2-Serial board

Features:
* Regular 3.3V UART on 2.54mm pinheader
* Does not need a crystal oscillator
* RX/TX debug LEDs
* LEDs on GPIOs `GP2` and `GP3`

![3D Rendering](/export/pl2303-breakout.png)

# Design considerations
This board was build around the parts available at the Physik IIIa workshop.
There is a full tube of the PL2303HXD chips.

Since the workshop does own a CNC-Mill (Protomat S103), the design is targeted 
for that manufacturing option.

# Software
* Linux just loads the driver out-of-the-box
* Windows needs to download it via Windows-Update, no manual install is required.
* GPIOs can be controlled via https://github.com/nekromant/pl2303gpio
